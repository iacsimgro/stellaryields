# Prototype functions of the supernova type II feedback
import numpy as np
import Anders89
from helpers import interpolate_log_space, dt_to_mass_range, ChabrierIMF, plot_ejecta_rates
import h5py


# Abundances is an array with the mass fraction of [H, He, C, N, O, Ne, Mg, Si, Fe]
def compute_supernovaII_feedback(M, M_lo, M_hi, abundances):
    Z_p = np.sum(abundances[2:])

    #print('Mass range: ', M_lo, M_hi)

    # We approximate the integral using the trapezoidal rule, as in general dt will be very low
    # In the case that M_lo==M_hi we can not do much I think...

    yields_lo, ejecta_lo = compute_SNII_yields(M_lo, Z_p)
    yields_hi, ejecta_hi = compute_SNII_yields(M_hi, Z_p)

    imf_lo = ChabrierIMF(M_lo)
    imf_hi = ChabrierIMF(M_hi)
    #print(imf_lo, imf_hi)

    # Doing this we would ignore the mass ejected that 'did not evolve'. 
    # So for example we will have negative ejected masses (because it is a yield now)
    #ejecta_hi *=0
    #ejecta_lo *=0

    # Doing this we would have passive evolution of the star
    #yields_lo *= 0
    #yields_hi *= 0


    # Trapezoidal rule (note, we have yields only for H, He, C, N, O)
    release_elements = M*0.5*( (yields_hi + abundances*ejecta_hi)*imf_hi
                              +(yields_lo + abundances*ejecta_lo)*imf_lo )*(M_hi-M_lo)
                       

    #print(release_elements)
    return release_elements



def compute_SNII_yields(M, Z_p):

    table_name = 'Tables/Portinari_table.hdf5'
    table = h5py.File(table_name, 'r')

    # Again, we interpolate first the metallicity
    z_index_lo, z_index_hi, dZ_lo, dZ_hi = interpolate_log_space(table['Z_bins'][...], Z_p)

    ejectas = ['H', 'He', 'C', 'N', 'O', 'Ne', 'Mg', 'Si', 'Fe']

    M_index_lo, M_index_hi, d_lo, d_hi = interpolate_log_space(table['Mass_bins'][...], M)

    # We compute the yield of each element, in solar masses
    # As we have the same mass sampling for all metallicities, we can do this more compact
    yields = np.zeros(len(ejectas))
    ejecta = table['Mej'][M_index_lo,z_index_lo]*d_lo*dZ_lo + table['Mej'][M_index_hi,z_index_lo]*d_hi*dZ_lo+\
             table['Mej'][M_index_lo,z_index_hi]*d_lo*dZ_hi + table['Mej'][M_index_hi,z_index_hi]*d_hi*dZ_hi
    i=0
    for el in ejectas:
        #yields[i] = table[el][M_index_lo,z_index_lo]*d_lo*dZ_lo + table[el][M_index_hi, z_index_lo]*d_hi*dZ_lo+\
        #            table[el][M_index_lo,z_index_hi]*d_lo*dZ_hi + table[el][M_index_hi, z_index_hi]*d_hi*dZ_hi
        yields[i] = table[el][M_index_lo,z_index_lo]*d_lo + table[el][M_index_hi, z_index_lo]*d_hi
        i+=1


    return yields, ejecta






if __name__=="__main__":
    import matplotlib.pyplot as plt
    
    t0 = 3.2e6 # approx 111 Mo
    tf = 6.5e7 # approx 6 Mo
    tt = np.logspace(np.log10(t0), np.log10(tf), 500)
    lt = np.zeros(500-1)
    #for i in range(500-1):
    #    lt[i],_ = dt_to_mass_range(tt[i],tt[i+1]-tt[i],0.004)

    #plt.plot(tt[:-1], lt)
    #plt.show()


    #compute_yields(4.87, 0.05)
    mm = np.logspace(np.log10(0.1),np.log10(100.), 50)
    print('IMF normalization: ', np.trapz(mm*ChabrierIMF(mm), mm))

    mm = np.logspace(np.log10(6),np.log10(120), 50)
    print('Mass between 6, 120 Mo', np.trapz(mm*ChabrierIMF(mm), mm))

    #plt.plot(mm, mm*ChabrierIMF(mm))
    #plt.show()

    m0 = 2e7
    Z0 = [0.0001, 0.0004, 0.0016, 0.0064, 0.025, 0.05]
    #Z0 = [0.0004]

    abun = Anders89.AndersAbundances('Tables/Anders89_abundances.dat', verbose=False)

    for z in Z0:

        abun.scale_metallicity(z)

        tt = np.logspace(np.log10(t0), np.log10(tf), 20)
        ## Now, lets do some plots
        released_elements = np.zeros((tt.shape[0]-1, 9))
        for i in range(tt.shape[0]-1):
            #print('%e'%tt[i])
            M_lo, M_hi = dt_to_mass_range(tt[i], tt[i+1]-tt[i], z)
            released_elements[i] = compute_supernovaII_feedback(m0, M_lo, M_hi, abun.get_all()) /(tt[i+1]-tt[i]) # We convert it to a rate





        plot_ejecta_rates(released_elements, tt, m0, z, save='imgs/%.4f.png'%z)
