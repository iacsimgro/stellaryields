import numpy as np
import h5py
from .stev_helpers import ChabrierIMF, linInterp, logInterp, nSNIa_exp, nSNIa_plaw

class StellarEvolution:
    def __init__(self, interp, IMF, DTD, precision):
        if interp == 'linear':
            self.InterpFunc = linInterp
        elif interp == 'log':
            self.InterpFunc = logInterp
        else:
            raise Exception('Unrecognized interpolation scheme.')
        
        if IMF == 'Chabrier':
            self.IMFFunc = ChabrierIMF
        else:
            raise Exception('Unrecognized IMF type.')
        
        if DTD == 'exp':
            self.nSNIaFunc = nSNIa_exp
        elif DTD == 'plaw':
            self.nSNIaFunc = nSNIa_plaw
        else:
            raise Exception('Unrecognized DTD type.')
        
        if precision == 'float64':
            self.fprec = np.float64
        elif precision == 'float128':
            self.fprec = np.float128
        else:
            self.fprec = np.float32
    
    
    def ComputeEjecta(self, times, simZ, simAbun, birthTime=0.0, initMass=1.0):
        assert isinstance(birthTime, (int,float))
        assert isinstance(initMass, (int,float))
        assert isinstance(simZ, (int,float))
        
        assert times.size > 1
        assert np.all(times[1:] > times[:-1])
        assert simAbun.size == self.nTracked - 1
        assert not np.any(simAbun < 0.0)
        
        if np.sum(simAbun) > 1.0:
            print('WARNING: The given abundances add up to more than one.')
        
        simAbun = np.asarray(simAbun, dtype=self.fprec)
        self.InputAbun = np.append(simAbun, simZ)
        self.InputTimes = np.asarray(times, dtype=self.fprec) - birthTime
        self.InputMasses = self.InverseLifetimeFunc(self.InputTimes, simZ)
        
        assert np.all(self.InputMasses[1:] <= self.InputMasses[:-1])
        
        self._interpIMFSamplesToZ(simZ)
        self._integrateEjectaAtIMFSamples()
        self._integrateEjectaAtInput()
        
        SNIaOnsetTime = self.LifetimeFunc(self.SNIa_MaxMass, simZ)
        nSNIa = self.nSNIaFunc(self.InputTimes, SNIaOnsetTime, self.DTD_norm, self.DTD_scale)
        self.EjectaSNIa = np.outer(nSNIa, self.Mej_SNIa)
    
    
    def InverseLifetimeFunc(self, times, Z):
        assert isinstance(Z, (int, float))
        
        if not isinstance(times, np.ndarray):
            times = np.array([times])
        masses = np.empty_like(times)
        
        iZ, dZ = self.InterpFunc(self.Z_Lifetimes, Z)
        if self.InterpLogY:
            for i in range(times.size):
                iT, dT = self.InterpFunc(self.Lifetimes[::-1,iZ], times[i])
                Mi_loZ = np.power(self.Mi_Lifetimes[::-1][iT], 1.0 - dT) * \
                         np.power(self.Mi_Lifetimes[::-1][iT+1], dT)
                
                iT, dT = self.InterpFunc(self.Lifetimes[::-1,iZ+1], times[i])
                Mi_hiZ = np.power(self.Mi_Lifetimes[::-1][iT], 1.0 - dT) * \
                         np.power(self.Mi_Lifetimes[::-1][iT+1], dT)
                
                masses[i] = np.power(Mi_loZ, 1.0 - dZ) * np.power(Mi_hiZ, dZ)
        
        else:
            for i in range(times.size):
                iT, dT = self.InterpFunc(self.Lifetimes[::-1,iZ], times[i])
                Mi_loZ = self.Mi_Lifetimes[::-1][iT] * (1.0 - dT) + \
                         self.Mi_Lifetimes[::-1][iT+1] * dT
                
                iT, dT = self.InterpFunc(self.Lifetimes[::-1,iZ+1], times[i])
                Mi_hiZ = self.Mi_Lifetimes[::-1][iT] * (1.0 - dT) + \
                         self.Mi_Lifetimes[::-1][iT+1] * dT
                
                masses[i] = Mi_loZ * (1.0 - dZ) + Mi_hiZ * dZ
        
        if times.size == 1:
            return masses[0]
        else:
            return masses
    
    
    def LifetimeFunc(self, masses, Z):
        assert isinstance(Z, (int, float))
        
        if not isinstance(masses, np.ndarray):
            masses = np.array([masses])
        times = np.empty_like(masses)
        
        iZ, dZ = self.InterpFunc(self.Z_Lifetimes, Z)
        for i in range(masses.size):
            iM, dM = self.InterpFunc(self.Mi_Lifetimes, masses[i])
            
            if self.InterpLogY:
                times[i] = np.power(self.Lifetimes[iM,iZ], (1.0 - dM) * (1.0 - dZ)) * \
                           np.power(self.Lifetimes[iM,iZ+1], (1.0 - dM) * dZ) * \
                           np.power(self.Lifetimes[iM+1,iZ], dM * (1.0 - dZ)) * \
                           np.power(self.Lifetimes[iM+1,iZ+1], dM * dZ)
            
            else:
                times[i] = self.Lifetimes[iM,iZ] * (1.0 - dM) * (1.0 - dZ) + \
                           self.Lifetimes[iM,iZ+1] * (1.0 - dM) * dZ + \
                           self.Lifetimes[iM+1,iZ] * dM * (1.0 - dZ) + \
                           self.Lifetimes[iM+1,iZ+1] * dM * dZ
        
        if masses.size == 1:
            return times[0]
        else:
            return times
    
    
    def _interpIMFSamplesToZ(self, Z):
        iZ, dZ = self.InterpFunc(self.Z_CCSN, Z)
        self.My_CCSN_Z  = self.My_CCSN[:,:,iZ] * (1.0 - dZ) + self.My_CCSN[:,:,iZ+1] * dZ
        self.Mej_CCSN_Z = self.Mej_CCSN[:,iZ] * (1.0 - dZ)  + self.Mej_CCSN[:,iZ+1]  * dZ
        
        iZ, dZ = self.InterpFunc(self.Z_AGB, Z)
        self.My_AGB_Z  = self.My_AGB[:,:,iZ] * (1.0 - dZ) + self.My_AGB[:,:,iZ+1] * dZ
        self.Mej_AGB_Z = self.Mej_AGB[:,iZ]  * (1.0 - dZ) + self.Mej_AGB[:,iZ+1]  * dZ 
    
    
    def _integrateEjectaAtIMFSamples(self):
        Ej_CCSN = self.My_CCSN_Z + np.outer(self.Mej_CCSN_Z, self.InputAbun)
        Ej_AGB  = self.My_AGB_Z  + np.outer(self.Mej_AGB_Z,  self.InputAbun)
        
        # Correct for negative values
        for i in range(self.nTracked - 1):
            negatives = Ej_CCSN[:,i] < 0.0
            if i > 1: Ej_CCSN[:,-1][negatives] -= Ej_CCSN[:,i][negatives]
            Ej_CCSN[:,i][negatives] = 0.0
            
            negatives = Ej_AGB[:,i] < 0.0
            if i > 1: Ej_AGB[:,-1][negatives] -= Ej_AGB[:,i][negatives]
            Ej_AGB[:,i][negatives] = 0.0
        
        negatives = Ej_CCSN[:,-1] < 0.0
        Ej_CCSN[:,-1][negatives] = 0.0
        
        negatives = Ej_AGB[:,-1] < 0.0
        Ej_AGB[:,-1][negatives] = 0.0
        
        assert not np.any(Ej_CCSN < 0.0)
        assert not np.any(Ej_AGB < 0.0)
        
        # Scale everything to match total ejecta predicted by the tables
        TotalEjecta = Ej_CCSN[:,0] + Ej_CCSN[:,1] + Ej_CCSN[:,-1]
        nonzeros = self.Mej_CCSN_Z > 0.0
        ScaleFactor = self.Mej_CCSN_Z[nonzeros] / TotalEjecta[nonzeros]
        Ej_CCSN[nonzeros,:] *= ScaleFactor[:,None]
        
        TotalEjecta = Ej_AGB[:,0] + Ej_AGB[:,1] + Ej_AGB[:,-1]
        nonzeros = self.Mej_AGB_Z > 0.0
        ScaleFactor = self.Mej_AGB_Z[nonzeros] / TotalEjecta[nonzeros]
        Ej_AGB[nonzeros,:] *= ScaleFactor[:,None]
        
        # Integrate along log mass
        logWeights = self.Masses * self.IMF
        Ej_CCSN *= logWeights[:,None]
        Ej_AGB  *= logWeights[:,None]
        
        self.EjectaCCSN_Samples = np.zeros_like(self.My_CCSN_Z)
        self.EjectaAGB_Samples  = np.zeros_like(self.My_AGB_Z)
        
        ln10 = np.log(10.0)
        iTrans = np.nonzero(self.Masses > self.CCSN_MinMass)[0][0]
        
        dLogMassTrans = np.log10(self.Masses[iTrans] / self.CCSN_MinMass)
        self.EjectaCCSN_Samples[iTrans:-1,:] = \
            0.5 * ln10 * self.dLogMass * (Ej_CCSN[iTrans:-1,:] + Ej_CCSN[iTrans+1:,:])
        self.EjectaCCSN_Samples[iTrans-1,:] = \
            0.5 * ln10 * dLogMassTrans * (Ej_CCSN[iTrans-1,:]  + Ej_CCSN[iTrans,:])
        
        dLogMassTrans = np.log10(self.CCSN_MinMass / self.Masses[iTrans-1])
        self.EjectaAGB_Samples[:iTrans-1,:] = \
            0.5 * ln10 * self.dLogMass * (Ej_AGB[:iTrans-1,:] + Ej_AGB[1:iTrans,:])
        self.EjectaAGB_Samples[iTrans-1,:] = \
            0.5 * ln10 * dLogMassTrans * (Ej_AGB[iTrans-1,:]  + Ej_AGB[iTrans,:])
    
    
    def _integrateEjectaAtInput(self):
        nInput = self.InputMasses.size
        self.EjectaCCSN = np.zeros((nInput, self.nTracked), dtype=self.fprec)
        self.EjectaAGB  = np.zeros((nInput, self.nTracked), dtype=self.fprec)
        
        for i in range(1, nInput):
            if self.InputMasses[i] < self.Masses[0]:
                iLower = 0
                fLower = 0.0
            elif self.InputMasses[i] > self.Masses[-1]:
                iLower = self.nIMF_Samples - 2
                fLower = 1.0
            else:
                iLower = np.nonzero(self.Masses < self.InputMasses[i])[0][-1]
                fLower = np.log10(self.InputMasses[i] / self.Masses[iLower]) / self.dLogMass
            
            if self.InputMasses[i-1] < self.Masses[0]:
                iUpper = 1
                fUpper = 1.0
            elif self.InputMasses[i-1] > self.Masses[-1]:
                iUpper = self.nIMF_Samples - 1
                fUpper = 0.0
            else:
                iUpper = np.nonzero(self.Masses < self.InputMasses[i-1])[0][-1] + 1
                fUpper = np.log10(self.Masses[iUpper] / self.InputMasses[i-1])/self.dLogMass
            
            self.EjectaCCSN[i,:] = np.sum(self.EjectaCCSN_Samples[iLower:iUpper,:], axis=0)
            self.EjectaAGB[i,:]  = np.sum(self.EjectaAGB_Samples[iLower:iUpper,:],  axis=0)
            
            self.EjectaCCSN[i,:] -= self.EjectaCCSN_Samples[iLower,:] * fLower + \
                                    self.EjectaCCSN_Samples[iUpper-1,:] * fUpper
            self.EjectaAGB[i,:]  -= self.EjectaAGB_Samples[iLower,:] * fLower + \
                                    self.EjectaAGB_Samples[iUpper-1,:] * fUpper



class Table(StellarEvolution):
    def __init__(self, path, interp, IMF, DTD, precision):
        super().__init__(interp, IMF, DTD, precision)
        
        self._readCCSN(path)
        self._readAGB(path)
        self._readSNIa(path)
        self._readLifetimes(path)
        
        self._sampleIMF()
        self._interpolateToIMFSamples()
    
    
    def _readCCSN(self, path):
        file = h5py.File(path + '/CCSN.hdf5', 'r')
        
        nMasses = file['Number_of_masses'][()]
        nZs     = file['Number_of_metallicities'][()]
        nElem   = file['Number_of_species'][()]
        yieldNames = list(file['Yield_names'].asstr())
        
        assert nZs == len(yieldNames)
        assert nElem == self.nTracked - 1
        
        self.Mi_CCSN  = np.array(file['Masses'], dtype=self.fprec)
        self.Z_CCSN   = np.array(file['Metallicities'], dtype=self.fprec)
        self.Mej_CCSN = np.empty((nMasses, nZs), dtype=self.fprec)
        self.My_CCSN  = np.empty((nMasses, self.nTracked, nZs), dtype=self.fprec)
        
        for k, name in enumerate(yieldNames):
            self.My_CCSN[:,:-1,k] = np.array(file['Yields/' + name + '/Yield'], 
                                             dtype=self.fprec).transpose()
            self.My_CCSN[:,-1,k]  = np.array(file['Yields/' + name + '/Total_Metals'], 
                                             dtype=self.fprec)
            self.Mej_CCSN[:,k]    = np.array(file['Yields/' + name + '/Ejected_mass'], 
                                             dtype=self.fprec)
        
        file.close()
    
    
    def _readAGB(self, path):
        file = h5py.File(path + '/AGB.hdf5', 'r')
        
        nMasses = file['Number_of_masses'][()]
        nZs     = file['Number_of_metallicities'][()]
        nElem   = file['Number_of_species'][()]
        yieldNames = list(file['Yield_names'].asstr())
        
        assert nZs == len(yieldNames)
        assert nElem == self.nTracked - 1
        
        self.Mi_AGB  = np.array(file['Masses'], dtype=self.fprec)
        self.Z_AGB   = np.array(file['Metallicities'], dtype=self.fprec)
        self.Mej_AGB = np.empty((nMasses, nZs), dtype=self.fprec)
        self.My_AGB  = np.empty((nMasses, self.nTracked, nZs), dtype=self.fprec)
        
        for k, name in enumerate(yieldNames):
            self.My_AGB[:,:-1,k] = np.array(file['Yields/' + name + '/Yield'], 
                                            dtype=self.fprec).transpose()
            self.My_AGB[:,-1,k]  = np.array(file['Yields/' + name + '/Total_Metals'], 
                                            dtype=self.fprec)
            self.Mej_AGB[:,k]    = np.array(file['Yields/' + name + '/Ejected_mass'], 
                                            dtype=self.fprec)
        
        file.close()
    
    
    def _readSNIa(self, path):
        file = h5py.File(path + '/SNIa.hdf5', 'r')
        
        nElem = file['Number_of_species'][()]
        assert nElem == self.nTracked - 1
        
        self.Mej_SNIa = np.empty(self.nTracked, dtype=self.fprec)
        self.Mej_SNIa[:-1] = np.array(file['Yield'], dtype=self.fprec)
        self.Mej_SNIa[-1]  = np.array(file['Total_Metals'], dtype=self.fprec)
        
        file.close()
    
    
    def _readLifetimes(self, path):
        file = h5py.File(path + '/Lifetimes.hdf5', 'r')
        
        self.Mi_Lifetimes = np.array(file['Masses'], dtype=self.fprec)
        self.Z_Lifetimes  = np.array(file['Metallicities'], dtype=self.fprec)
        self.Lifetimes    = np.array(file['Lifetimes'], dtype=self.fprec).transpose()
        
        assert self.Lifetimes.shape[0] == self.Mi_Lifetimes.size
        assert self.Lifetimes.shape[1] == self.Z_Lifetimes.size
        
        file.close()
    
    
    def _sampleIMF(self):
        minMass = self.Mi_Lifetimes[0]
        
        self.dLogMass = np.log10(self.IMF_MaxMass / minMass) / (self.nIMF_Samples - 1)
        self.Masses = np.logspace(np.log10(minMass), np.log10(self.IMF_MaxMass), 
                                  num=self.nIMF_Samples, dtype=self.fprec)
        self.IMF = self.IMFFunc(self.Masses)
    
    
    def _interpolateToIMFSamples(self):
        iTrans = np.nonzero(self.Masses > self.CCSN_MinMass)[0][0]
        
        My_AGB  = np.zeros((self.nIMF_Samples, self.nTracked, self.Z_AGB.size), 
                           dtype=self.fprec)
        Mej_AGB = np.zeros((self.nIMF_Samples, self.Z_AGB.size), dtype=self.fprec)
        for i in range(iTrans + 1):
            iM, dM = self.InterpFunc(self.Mi_AGB, self.Masses[i])
            My_AGB[i,:,:] = self.My_AGB[iM,:,:] * (1.0 - dM) + self.My_AGB[iM+1,:,:] * dM
            Mej_AGB[i,:]  = self.Mej_AGB[iM,:]  * (1.0 - dM) + self.Mej_AGB[iM+1,:]  * dM
        
        assert np.all(np.isfinite(My_AGB))
        assert np.all(np.isfinite(Mej_AGB))
        
        self.My_AGB  = My_AGB
        self.Mej_AGB = Mej_AGB
        
        
        My_CCSN  = np.zeros((self.nIMF_Samples, self.nTracked, self.Z_CCSN.size), 
                            dtype=self.fprec)
        Mej_CCSN = np.zeros((self.nIMF_Samples, self.Z_CCSN.size), dtype=self.fprec)
        for i in range(iTrans - 1, self.nIMF_Samples):
            iM, dM = self.InterpFunc(self.Mi_CCSN, self.Masses[i])
            My_CCSN[i,:,:] = self.My_CCSN[iM,:,:] * (1.0 - dM) + self.My_CCSN[iM+1,:,:] * dM
            Mej_CCSN[i,:]  = self.Mej_CCSN[iM,:]  * (1.0 - dM) + self.Mej_CCSN[iM+1,:]  * dM
        
        assert np.all(np.isfinite(My_CCSN))
        assert np.all(np.isfinite(Mej_CCSN))
        
        self.My_CCSN  = My_CCSN
        self.Mej_CCSN = Mej_CCSN


class Standard(Table):
    '''
    ~~~  Standard model in <InsertFancyCodeName>  ~~~
    
    CCSN      -> Portinari et al. 1998 (A&A 334, 505-539)
    AGB       -> Marigo et al. 2001 (A&A 370, 194-217)
    SNIa      -> Thielemann et al. 2003 (Proc. ESO/MPA/MPE Workshop, Garching, Germany, 331)
    Lifetimes -> Portinari et al. 1998 (A&A 334, 505-539)
    '''
    def __init__(self, path, interp='log', IMF='Chabrier', DTD='exp', precision='float32'):
        self.trackedList = ['Hydrogen', 'Helium', 'Carbon', 'Nitrogen', 'Oxygen', 'Neon', 
                            'Magnesium', 'Silicon', 'Iron', 'Total metals']
        
        self.InterpLogY = True
        
        if DTD == 'exp':
            self.DTD_norm  = 2e-3   # dSNIa_Norm
            self.DTD_scale = 2e9    # dSNIa_Scale
        elif DTD == 'plaw':
            N  = 1.3e-3             # dSNIa_Norm
            s  = -1.1               # dSNIa_Scale
            ti = 40e6               # dSNIa_Norm_ti
            tH = 13.7e9             # dSNIa_Norm_tf
            norm = N / (tH**(s + 1.0) - ti**(s + 1.0))
            self.DTD_norm  = norm
            self.DTD_scale = s
        
        self.IMF_MinMass  = 0.1     # dIMF_MinMass
        self.IMF_MaxMass  = 100.0   # dIMF_MaxMass
        self.CCSN_MinMass = 6.0     # dCCSN_MinMass
        self.SNIa_MaxMass = 8.0     # dSNIa_MaxMass
        self.nIMF_Samples = 200     # STEV_INTERP_N_MASS
        
        
        self.nTracked = len(self.trackedList)
        super().__init__(path, interp, IMF, DTD, precision)



if __name__ == '__main__':
    pass
