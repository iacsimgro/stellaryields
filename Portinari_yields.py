# The table 10 of Portinari gives total mass ejecta, we need to convert that to a yield
# The yield is the difference between what was ejected, and what would be ejected if nop nucleoshintesis took place, i.e., the stellar composition does not change
import numpy as np
import Anders89
import h5py

# We load the default abundances of Anders & Grevesse
abundances = Anders89.AndersAbundances('Tables/Anders89_abundances.dat', verbose=True)

# We read the Portinari table
Z,M,H1,He3,He4,C12,C13,N14,N15,O16,O17,O18,Ne20,Ne22,Mg24,Si28,S32,Ca40,Fe56,Mr=\
        np.genfromtxt('Tables/Portinari98_yield_massives_star_winds.txt', unpack=True)

Zs = np.unique(Z)
n_Zs = Zs.shape[0]
Ms = np.unique(M)
n_Ms = Ms.shape[0]

elems = [H1, He3+He4, C12+C13, N14+N15, O16+O17+O18, Ne20+Ne22, Mg24, Si28, Fe56, M-Mr]
elem_names = ['H', 'He', 'C', 'N', 'O', 'Ne', 'Mg', 'Si', 'Fe', 'Mej']

out = h5py.File('Tables/Portinari_table.hdf5', 'w')

out.create_dataset('Mass_bins', data=Ms)
out.create_dataset('Z_bins', data=Zs)

for el,name in zip(elems,elem_names):
    print(name)

    el_yield = np.zeros((n_Ms, n_Zs))

    m_index = 0
    for M0 in Ms:
        mask_m = M==M0
        z_index = 0
        for Z0 in Zs:
            mask_z = Z==Z0


            total_ejecta =  el[mask_m & mask_z]

            # Safety check
            # Again, I have no idea why, but they change the mass samples between metallicities!! AHHG
            if (len(total_ejecta)!=1):
                #print("Something went wrong...", M0, Z0)
                el_yield[m_index, z_index] = -1
                z_index+=1
                continue

            total_ejecta = total_ejecta[0]
                
            if name!='Mej':
                # We calculate the expected ejecta assuming no composition change
                abundances.scale_metallicity(Z0, verbose=False)
                mejecta = elems[-1][mask_m & mask_z] #M0*elems[-1][mask_m & mask_z][0]
                ejecta_nonucleo = abundances.get(name)*mejecta

                el_yield[m_index, z_index] = (total_ejecta-ejecta_nonucleo) #/M0
                if total_ejecta==-1:
                    print('No yield')
                    el_yield[m_index, z_index] = 0.

                #if name=='Si':
                #    print (m_index, z_index, total_ejecta,ejecta_nonucleo)
            else:
                el_yield[m_index, z_index] = el[mask_m & mask_z][0]

            z_index+=1
        m_index+=1

        
    # The cases that are missing are: 
    #  30 0.05 ; 60 0.004 ; 100 0.008
    # We will interpolate at those masses at fixed metallicity
    m_index = np.argwhere(Ms==30)
    z_index = np.argwhere(Zs==0.05)
    el_yield[m_index, z_index] = 0.5*(el_yield[m_index+1,z_index] + el_yield[m_index-1, z_index])

    # In this case the other masses are not at the same distance!
    m_index = np.argwhere(Ms==60)
    z_index = np.argwhere(Zs==0.004)
    el_yield[m_index, z_index] = el_yield[m_index+1,z_index]/3. + el_yield[m_index-1, z_index]*2./3.

    m_index = np.argwhere(Ms==100)
    z_index = np.argwhere(Zs==0.008)
    el_yield[m_index, z_index] = 2.*el_yield[m_index+1,z_index]/3. + el_yield[m_index-1, z_index]/3.
    out.create_dataset(name, data=el_yield)


out.close()

# Safety check: repeat Fig 5 and 6 of Portinari
table = h5py.File('Tables/Portinari_table.hdf5', 'r')

Z = 0.0004
z_index = np.argwhere(table['Z_bins'][...]==Z)

# To be compared with Figure 6, change Z to compare with other plots
import matplotlib.pyplot as plt
plt.plot(table['Mass_bins'], 1.-table['Mej'][:,z_index]/table['Mass_bins'], 'k-', label='RM')
plt.plot(table['Mass_bins'], table['He'][:,z_index]/table['Mass_bins'], 'k--', label='He')
plt.plot(table['Mass_bins'], table['O'][:,z_index]/table['Mass_bins'], 'k-.', label='O')
plt.plot(table['Mass_bins'], table['C'][:,z_index]/table['Mass_bins'], 'k:', label='C')

plt.ylim(0,0.5)
plt.xlabel('Mass [M$_\odot$]')
plt.ylabel('Relative yield')
plt.title("Z=%.4f"%Z)
plt.legend(loc=0)

plt.show()

