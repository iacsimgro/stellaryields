# Stellar yields

This repository is a quick Python prototype of the stellar feedback that will be eventually developed in my fork of PKDGRAV3.

This implementation follows [Wiersma et al., 2009](https://doi.org/10.1111/j.1365-2966.2009.15331.x), making use of the same tables:

* Solar abundances [Anders & Gevesse, 1989](https://doi.org/10.1016/0016-7037(89)90286-X). These are then scaled to the desired metal mass fraction, Z, scaling the helium mass fraction as Y = 0.23 + 2.25 Z. The class [Anders89](Anders89.py) handles the initialization and scaling, and the table itself is located [here](Tables/Anders89_abundances.dat).
* Metallicity-dependent stellar lifetimes are extracted from [Portinari et al., 1998](https://ui.adsabs.harvard.edu/abs/1998A%26A...334..505P/abstract), Table 14. These tables are interpolated to obtain the mass range of the stars that ended the main-sequence between t and t+dt. This can be found in [helpers.py](helpers.py): `dt_to_mass_range`. 
* Supernova Type II yields are also extracted from [Portinari et al., 1998](https://ui.adsabs.harvard.edu/abs/1998A%26A...334..505P/abstract), Tables 7 and 10. These cover a mass range of 6 to 120 solar masses. However, in these tables the *total* ejecta is given, to convert it to a yield, we must substract the expected ejecta if there was no nucleosynthesis. This is done in [preproccesing](Portinari_yields.py), and then a new HDF5 is generated with the yields (in solar mass units).
* AGB winds yields are computed in [Marigo, 2001](https://doi.org/10.1051/0004-6361:20000247). The tables were downloaded from the [Padova website](http://pleiadi.pd.astro.it/), and only the relevant ones are incorporated in this repository. In this case the tables already give the yield, so there is no need to preprocess them.

At the end of the day, we want to obtain the quantity of elements ejected from a single stellar population, which will be added to the neighbouring gas particles:

![yields][yieldeq]

where ![yi][yi] is the yield of the i-th element in units of solar mass, ![mej][mej] the ejected mass to the ISM and ![phi][phi] the Initial Mass Function (IMF), which we assumed to be a [Chabrier IMF](https://ui.adsabs.harvard.edu/abs/2003PASP..115..763C/abstract). The time is measured from the birth of the SSP, and the integral's mass range indicates the masses of the stars that ended their main-sequence between t and t+dt. The post main sequence evolution is assumed to be instantaneous.

The main file is [feedback.py](feedback.py), which generates some plots of the mass ejected per year for each element, and the cumulative mass ejected.
The AGB and SNII feedback are computed independetly in [AGB_feedback](AGB_feedback.py) and [supernovaII_feedback.py](supernovaII_feedback.py)


![example](imgs/0.0064.png "Example")

[yieldeq]: https://latex.codecogs.com/gif.download?%5CDelta%20M_%7Bi%7D%20%28t%2C%20dt%2C%20Z%29%20%3D%20%5Cint_%7BM+dM%7D%5E%7BM%7D%20%28y_i%20+%20m_%7Bej%7DZ_%7Bi%7D%29%5CPhi%28m%29%20dm%2C "yield"
[yi]: https://latex.codecogs.com/gif.download?y_i "yi"
[mej]: https://latex.codecogs.com/gif.download?m_%7Bej%7D "mej"
[phi]: https://latex.codecogs.com/gif.download?%5CPhi%20%28m%29 "phi"
