import numpy as np

def ChabrierIMF(M, Mc=0.079, sigma=0.69, M0=0.1, Mf=100, A=0.84975):
    # Stupid fix to python non-sense :)
    if type(M)==float:
        M = np.array([M])
    B = A*np.exp(-(-np.log10(Mc))**2/(2.*sigma**2) )
    mask = M<1.

    IMF = np.zeros_like(M)
    IMF[mask] = A*np.exp(-(np.log10(M[mask])-np.log10(Mc))**2/(2.*sigma**2) )/M[mask]
    mask = np.invert(mask)
    IMF[mask] = B*M[mask]**(-2.3)

    return IMF

def interpolate_log_space(x, x_p):
    ## CAUTION! We assume the x array to be sorted!
    max_index = x.shape[0]

    # We interpolate between different metallicites
    index_lo = 0
    index_hi = 0
    while index_hi<max_index and (x_p>x[index_hi]) :
        index_hi += 1

    if index_hi>0:
        index_lo = index_hi-1
    
    # If we are above 0.05
    if index_hi == max_index:
        index_hi = max_index-1
        index_lo = max_index-1


    # We compute the weight for each table, we do so in log space
    dx_lo = np.log10(x_p) - np.log10(x[index_lo]) 
    dx_hi = np.log10(x[index_hi]) - np.log10(x_p)


    # We take special care of out-of-bounds interpolations and the cases when we have the exact same tabulated metallicity
    if dx_lo==0 or index_hi==0:
        dx_lo = 1.
        dx_hi = 0.
    elif dx_hi==0 or index_lo==max_index-1:
        dx_hi = 1.
        dx_lo = 0.
    else: # Standard case, inverse distance weights
        dx_norm = 1./dx_lo + 1./dx_hi
        dx_lo = 1./dx_lo / dx_norm
        dx_hi = 1./dx_hi / dx_norm
        #dx_norm = np.log10(x[index_hi]) - np.log10(x[index_lo]) #1./dx_lo + 1./dx_hi
        #dx_lo = dx_hi/dx_norm #1./dx_lo / dx_norm
        #dx_hi = 1.-dx_lo #dx_lo/dx_norm #1./dx_hi / dx_norm

    #print (index_lo, index_hi, dx_lo,dx_hi )
    return index_lo, index_hi, dx_lo,dx_hi 

def dt_to_mass_range(t, dt, Z_p):
    # First, compute the mass ranges given by t, t+dt
    M_lt, lt_Z0004, lt_Z004, lt_Z008, lt_Z02, lt_Z05 = np.loadtxt('Tables/Portinari98_lifetime.dat', unpack=True)
    Zs_lt = np.array([0.0004, 0.004, 0.008, 0.02, 0.05])
    lifetimes = [lt_Z0004, lt_Z004, lt_Z008, lt_Z02, lt_Z05]

    # We interpolate between different metallicites
    z_index_lo, z_index_hi, dZ_lo, dZ_hi = interpolate_log_space(Zs_lt, Z_p)

    
    # Having done this, we interpolate the tables

    # low metallicity table
    M0_lo, M0_hi, dx_lo, dx_hi = interpolate_log_space(lifetimes[z_index_lo][::-1], t)
    M0_loz = M_lt[::-1][M0_lo]*dx_lo + M_lt[::-1][M0_hi]*dx_hi

    Mdt_lo, Mdt_hi, dx_lo, dx_hi = interpolate_log_space(lifetimes[z_index_lo][::-1], t+dt)
    Mdt_loz = M_lt[::-1][Mdt_lo]*dx_lo + M_lt[::-1][Mdt_hi]*dx_hi

    # high metallicity table
    M0_lo, M0_hi, dx_lo, dx_hi = interpolate_log_space(lifetimes[z_index_hi][::-1], t)
    M0_hiz = M_lt[::-1][M0_lo]*dx_lo + M_lt[::-1][M0_hi]*dx_hi

    Mdt_lo, Mdt_hi, dx_lo, dx_hi = interpolate_log_space(lifetimes[z_index_hi][::-1], t+dt)
    Mdt_hiz = M_lt[::-1][Mdt_lo]*dx_lo + M_lt[::-1][Mdt_hi]*dx_hi

    # Metallicity weighting
    M0 = M0_loz*dZ_lo + M0_hiz*dZ_hi
    Mdt = Mdt_loz*dZ_lo + Mdt_hiz*dZ_hi

    #print (M0, Mdt)
    return Mdt, M0

def plot_ejecta_rates(ejecta_rates, tt, m0, Z, save=None):
    import matplotlib.pyplot as plt
    f, ax = plt.subplots(1,2, figsize=(10,5))
    ax[0].plot(tt[:-1], ejecta_rates[:,0], label='H')
    ax[0].plot(tt[:-1], ejecta_rates[:,1], label='He')
    ax[0].plot(tt[:-1], ejecta_rates[:,2], label='C')
    ax[0].plot(tt[:-1], ejecta_rates[:,3], label='N')
    ax[0].plot(tt[:-1], ejecta_rates[:,4], label='O')
    ax[0].plot(tt[:-1], ejecta_rates[:,5], label='Ne')
    ax[0].plot(tt[:-1], ejecta_rates[:,6], label='Mg')
    ax[0].plot(tt[:-1], ejecta_rates[:,7], label='Si')
    ax[0].plot(tt[:-1], ejecta_rates[:,8], label='Fe')
    #ax[0].set_xlim(1e8, 1e10)
    ax[0].set_ylim(1e-8,1)
    ax[0].loglog()
    ax[0].set_xlabel('Time after star formation [yr]')
    ax[0].set_ylabel(r'Ejected mass [M$_\odot$ yr$^{-1}$]')
    ax[0].set_title('Initial mass %.1e ; Z=%.4f'%(m0, Z))


    total_ejected = np.sum(ejecta_rates,axis=1)
    total_ejected = np.cumsum(total_ejected*np.diff(tt))

    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,0]*np.diff(tt)), label='H')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,1]*np.diff(tt)), label='He')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,2]*np.diff(tt)), label='C')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,3]*np.diff(tt)), label='N')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,4]*np.diff(tt)), label='O')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,5]*np.diff(tt)), label='Ne')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,6]*np.diff(tt)), label='Mg')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,7]*np.diff(tt)), label='Si')
    ax[1].plot(tt[:-1], np.cumsum(ejecta_rates[:,8]*np.diff(tt)), label='Fe')
    ax[1].plot(tt[:-1], total_ejected, 'k', label='Total ejected')
    #ax[1].set_xlim(1e8, 1e10)
    ax[1].set_ylim(1e1,2e7)
    ax[1].loglog()
    ax[1].set_xlabel('Time after star formation [yr]')
    ax[1].set_ylabel(r'Cumulative ejected mass [M$_\odot$]')
    ax[1].set_title('Fraction of ejected mass: %.5f'%(total_ejected[-1]/m0) )


    ax[1].legend(loc=0, ncol=2)

    if save!=None:
        f.savefig(save)
    else:
        plt.show()

