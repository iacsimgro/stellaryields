import numpy as np
from scipy.special import erf

def ChabrierIMF(M, M0=0.1, Mf=100.0):
    if not isinstance(M, np.ndarray):
        M = np.array([M])
    
    Mc = 0.079
    sigma = 0.69
    slope = -2.3
    
    ln10  = np.log(10.0)
    logMc = np.log10(Mc)
    C = np.exp(-0.5 * (logMc / sigma)**2)
    
    I1 = np.sqrt(0.5 * np.pi) * np.exp(0.5 * (sigma * ln10)**2) * ln10 * sigma * Mc * \
         (erf((-logMc / sigma - ln10 * sigma) / np.sqrt(2.0)) - 
          erf(((np.log10(M0) - logMc) / sigma - ln10 * sigma) / np.sqrt(2.0)))
    I2 = C * (Mf**(slope + 2.0) - 1.0) / (slope + 2.0)
    
    A = 1.0 / (I1 + I2)
    B = A * C
    
    mask = M < 1.0
    
    IMF = np.empty_like(M)
    IMF[mask] = A * np.exp(-(np.log10(M[mask]) - np.log10(Mc))**2/(2.0 * sigma**2) ) / M[mask]
    mask = np.logical_not(mask)
    IMF[mask] = B * M[mask]**(slope)
    
    if IMF.size == 1:
        return IMF[0]
    else:
        return IMF


def linInterp(x, x_p):
    ## CAUTION! We assume the x array to be sorted in increasing order!
    if x_p < x[0]:
        ix = 0
        dx = 0.0
    elif x_p > x[-1]:
        ix = x.shape[0] - 2
        dx = 1.0
    else:
        for ix in range(1, x.shape[0]):
            if x[ix] > x_p: break
        ix -= 1
        dx  = (x_p - x[ix]) / (x[ix + 1] - x[ix])
    
    return ix, dx


def logInterp(x, x_p):
    ## CAUTION! We assume the x array to be sorted in increasing order!
    if x_p < x[0]:
        ix = 0
        dx = 0.0
    elif x_p > x[-1]:
        ix = x.shape[0] - 2
        dx = 1.0
    else:
        for ix in range(1, x.shape[0]):
            if x[ix] > x_p: break
        ix -= 1
        dx  = (np.log10(x_p) - np.log10(x[ix])) / (np.log10(x[ix + 1]) - np.log10(x[ix]))
    
    return ix, dx


def nSNIa_exp(times, SNIaOnsetTime, norm, scale):
    assert times.size > 1
    assert np.all(times[1:] > times[:-1])
    
    nSNIa = np.zeros_like(times)
    for i in range(1, times.size):
        t0, tf = times[i-1], times[i]
        if tf < SNIaOnsetTime:
            continue
        elif t0 < SNIaOnsetTime:
            t0 = SNIaOnsetTime
        
        nSNIa[i] = norm * (np.exp(-(t0 / scale)) - np.exp(-(tf / scale)))
    
    return nSNIa


def nSNIa_plaw(times, SNIaOnsetTime, norm, scale):
    assert times.size > 1
    assert np.all(times[1:] > times[:-1])
    
    nSNIa = np.zeros_like(times)
    for i in range(1, times.size):
        t0, tf = times[i-1], times[i]
        if tf < SNIaOnsetTime:
            continue
        elif t0 < SNIaOnsetTime:
            t0 = SNIaOnsetTime
        
        nSNIa[i] = norm * (tf**(scale + 1.0) - t0**(scale + 1.0))
    
    return nSNIa

if __name__ == '__main__':
    pass
