# Test of the abundances extracted from Anders & Gevesse
import numpy as np


class AndersAbundances():
    index = {'H':0, 'He':1, 'C':2, 'N':3, 'O':4, 'Ne':5, 'Mg':6, 'Si':7, 'Fe':8}

    def __init__(self, table_file, verbose=False):
        dtype = [('el',  '|S2'), ('A', float), ('A_Si', float)]
        self.table = np.genfromtxt(table_file, dtype=dtype)

        # Convert to particle number relative to H
        #N = np.exp(table['A']-12.)
        N = self.table['A_Si']/self.table['A_Si'][0]

        # And then to mass fractions (from Anders & Grevesse, table 3)
        mH  = 99.9966 + 0.0034*2
        mHe = 3*0.0142 + 4.*99.9858
        mC  = 12*98.90 + 13*1.1
        mN  = 14*99.634 + 15*0.366
        mO  = 16*99.762 + 17*0.038 + 18*0.2
        mNe = 20*92.99 + 21*0.226 + 22*6.79
        mMg = 24*78-99 + 25*10. + 26*11.01
        mSi = 28*92.23 + 29*4.67 + 30*3.1
        mFe = 54*5.8 + 56*91.72 + 57*2.2 + 58*0.28

        N[0] *=  mH  / 100.
        N[1] *=  mHe / 100.
        N[2] *=  mC  / 100.
        N[3] *=  mN  / 100.
        N[4] *=  mO  / 100.
        N[5] *=  mNe / 100.
        N[6] *=  mMg / 100.
        N[7] *=  mSi / 100.
        N[8] *=  mFe / 100.

        mtot = np.sum(N)

        self.N = N/mtot

        if verbose:
            for i in range(len(N)):
                print(self.table['el'][i], self.N[i])
            print('Z=', np.sum(self.N[2:]))

    def scale_metallicity(self, Z_final, verbose=False):
        # First we compute the current metallicity
        Z_now = np.sum(self.N[2:])

        # Factor to apply to metals
        f = Z_final/Z_now

        self.N[2:] *= f
        # We assume Y = 0.23 + 2.25*Z (Vazdekis 2010, MNRAS 404, 1639)
        self.N[1] = 0.23 + 2.25*np.sum(self.N[2:])
        self.N[0] = 1-np.sum(self.N[1:])
        #self.N /= np.sum(self.N)

        if verbose:
            for i in range(len(self.N)):
                print(self.table['el'][i], self.N[i])
            print('Z=', np.sum(self.N[2:]))
            print('X/Y=', self.N[0]/self.N[1])
            print('X+Y+Z=',np.sum(self.N))

    def get(self, element):
        return self.N[self.index[element]]

    def get_all(self):
        return self.N

if __name__=='__main__':
    # Do some testing

    print("\tCreating abundances table...")
    abundances = AndersAbundances('Tables/Anders89_abundances.dat', verbose=True)

    print("\n\tRescaling metallicity to Z=0.004")
    abundances.scale_metallicity(0.004, verbose=True)
