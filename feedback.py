import numpy as np
from helpers import ChabrierIMF, dt_to_mass_range, plot_ejecta_rates
from AGB_feedback import compute_AGB_feedback
from supernovaII_feedback import compute_supernovaII_feedback
import Anders89

## This is the master feedback routine, which will call AGB or supernova feedback depending on the stellar time
def compute_all_feedback(M, t, dt, abundances):
    Z_p = np.sum(abundances[2:])

    # These are the transition masses (times) between SNII and AGB feedbacks
    M_trans = 6. #Mo

    M_lo, M_hi = dt_to_mass_range(t, dt, Z_p)


    released_elements = np.zeros(9)
    if M_lo > M_trans: # Pure SNII feedback
        released_elements = compute_supernovaII_feedback(M, M_lo, M_hi, abundances)
    elif M_hi < M_trans: # Pure AGB feedback
        released_elements = compute_AGB_feedback(M, M_lo, M_hi, abundances)
    else: # Mixed SNII+AGB feedback
        released_AGB_elements = compute_AGB_feedback(M, M_lo, M_trans, abundances)
        released_SNII_elements = compute_supernovaII_feedback(M, M_trans, M_hi, abundances)
        released_elements = released_AGB_elements + released_SNII_elements

    return released_elements




if __name__=="__main__":
    mm = np.logspace(np.log10(0.1),np.log10(100.), 50)
    print('IMF normalization: ', np.trapz(mm*ChabrierIMF(mm), mm))

    mm = np.logspace(np.log10(0.8),np.log10(100.), 50)
    print('Mass between 0.8, 100 Mo', np.trapz(mm*ChabrierIMF(mm), mm))

    m0 = 2e7
    Z0 = [0.0001, 0.0004, 0.0016, 0.0064, 0.025, 0.05]
    #Z0 = [0.025]

    abun = Anders89.AndersAbundances('Tables/Anders89_abundances.dat', verbose=False)

    for z in Z0:

        abun.scale_metallicity(z)

        t0 = 3.2e6 # approx 4.8 Mo
        tf = 1e10
        tt = np.logspace(np.log10(t0), np.log10(tf), 100)
        ## Now, lets do some plots
        released_elements = np.zeros((tt.shape[0]-1, 9))
        for i in range(tt.shape[0]-1):
            #print('%e'%tt[i])
            released_elements[i] = compute_all_feedback(m0, tt[i], tt[i+1]-tt[i], abun.get_all()) /(tt[i+1]-tt[i]) # We convert it to a rate





        plot_ejecta_rates(released_elements, tt, m0, z, save='imgs/%.4f.png'%z)

