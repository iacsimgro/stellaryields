# Prototype functions of the AGB feedback 
import numpy as np
import Anders89
from helpers import interpolate_log_space, dt_to_mass_range, ChabrierIMF, plot_ejecta_rates

# Abundances is an array with the mass fraction of [H, He, C, N, O, Ne, Mg, Si, Fe]
def compute_AGB_feedback(M, M_lo, M_hi, abundances):
    #print('Input abundances: ', abundances)
    Z_p = np.sum(abundances[2:])
    #print('Input metallicity: ', Z_p)

    #print('Mass range: ', M_lo, M_hi)

    # We approximate the integral using the trapezoidal rule, as in general dt will be very low
    # In the case that M_lo==M_hi we can not do much I think...

    yields_lo, ejecta_lo = compute_AGB_yields(M_lo, Z_p)
    yields_hi, ejecta_hi = compute_AGB_yields(M_hi, Z_p)

    imf_lo = ChabrierIMF(M_lo)
    imf_hi = ChabrierIMF(M_hi)
    #print(imf_lo, imf_hi)

    # Doing this we would ignore the mass ejected that 'did not evolve'. So for example we will have negative ejected masses (because is a yield now)
    #ejecta_hi *=0
    #ejecta_lo *=0

    # Doing this we would have passive evolution of the star
    #yields_lo *= 0
    #yields_hi *= 0

    #print(yields_lo, yields_hi)

    # Trapezoidal rule (note, we have yields only for H, He, C, N, O
    #print(M_lo, ejecta_lo, M_lo-M_hi, imf_hi)
    release_elements = np.zeros(9)
    release_elements[:-4] = M*0.5*( (yields_hi + abundances[:-4]*ejecta_hi)*imf_hi
                                   +(yields_lo + abundances[:-4]*ejecta_lo)*imf_lo )*(M_hi-M_lo)
    release_elements[-4:] = M*0.5*( ( abundances[-4:]*ejecta_hi)*imf_hi
                                    +( abundances[-4:]*ejecta_lo)*imf_lo )*(M_hi-M_lo)
                       

    return release_elements




def compute_AGB_yields(M, Z_p):

    Zs_marigo = np.array([0.004, 0.008, 0.019])
    totyields_files = ['totyieldsz004.dat', 'totyieldsz008.dat', 'totyieldsz019.dat']
    # Again, we interpolate first the metallicity
    z_index_lo, z_index_hi, dZ_lo, dZ_hi = interpolate_log_space(Zs_marigo, Z_p)

    # We read the yields from the low metallicity table
    table = np.loadtxt('Tables/'+totyields_files[z_index_lo], skiprows=1)
    table = table.T
    #alpha_MLT Mi    Mej      My(H)     My(3He)   My(4He)   My(12C)    My(13C)   My(14N)   My(15N)    My(16O)    My(17O)   My(18O)   My(CNO)
    ejectas = [table[3], table[4]+table[5], table[6]+table[7], table[8]+table[9], table[10]+table[11]+table[12]]

    M_index_lo, M_index_hi, d_lo, d_hi = interpolate_log_space(table[1], M)

    # We compute the yield of each element, in solar masses
    yields_lo = np.zeros(len(ejectas))
    ejecta_lo = table[2][M_index_lo]*d_lo + table[2][M_index_hi]*d_hi
    for i in range(len(ejectas)):
        yields_lo[i] = (ejectas[i][M_index_lo]*d_lo + ejectas[i][M_index_hi]*d_hi)

    # We repeat for the high metallicity table  (this can be done a lot better!)
    table = np.loadtxt('Tables/'+totyields_files[z_index_hi], skiprows=1)
    table = table.T
    M_index_lo, M_index_hi, d_lo, d_hi = interpolate_log_space(table[1], M)
    ejectas = [table[3], table[4]+table[5], table[6]+table[7], table[8]+table[9], table[10]+table[11]+table[12]]
    yields_hi = np.zeros(len(ejectas))
    ejecta_hi = table[2][M_index_lo]*d_lo + table[2][M_index_hi]*d_hi
    for i in range(len(ejectas)):
        yields_hi[i] = (ejectas[i][M_index_lo]*d_lo + ejectas[i][M_index_hi]*d_hi)

    yields = yields_lo*dZ_lo + yields_hi*dZ_hi
    ejecta = ejecta_lo*dZ_lo + ejecta_hi*dZ_hi

    return yields, ejecta





if __name__=="__main__":
    #dt_to_mass_range(5e6,1e5,0.006)
    #compute_yields(4.87, 0.05)
    mm = np.logspace(np.log10(0.1),np.log10(100.), 50)
    print('IMF normalization: ', np.trapz(mm*ChabrierIMF(mm), mm))

    mm = np.logspace(np.log10(0.8),np.log10(5), 50)
    print('Mass between 0.8, 5 Mo', np.trapz(mm*ChabrierIMF(mm), mm))

    #import matplotlib.pyplot as plt
    #plt.plot(mm, mm*ChabrierIMF(mm))
    #plt.show()

    m0 = 2e7
    Z0 = [0.0001, 0.0004, 0.0016, 0.0064, 0.025, 0.05]
    #Z0 = [0.005]

    abun = Anders89.AndersAbundances('Tables/Anders89_abundances.dat', verbose=False)

    for z in Z0:

        abun.scale_metallicity(z)

        t0 = 1.2e8 # approx 4.8 Mo
        tf = 1e10
        tt = np.logspace(np.log10(t0), np.log10(tf), 100)
        ## Now, lets do some plots
        released_elements = np.zeros((tt.shape[0]-1, 9))
        for i in range(tt.shape[0]-1):
            #print('%e'%tt[i])
            M_lo, M_hi = dt_to_mass_range(tt[i], tt[i+1]-tt[i], z)
            released_elements[i] = compute_AGB_feedback(m0, M_lo, M_hi, abun.get_all()) /(tt[i+1]-tt[i]) # We convert it to a rate





        plot_ejecta_rates(released_elements, tt, m0, z, save='imgs/%.4f.png'%z)


